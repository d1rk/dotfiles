## About

This is dotfiles for Tasuku SUENAGA a.k.a. gunyarakun.

## Install


  ```
  $ git clone https://github.com/gunyarakun/dotfiles.git ~/dotfiles
  ```

2. bootstrap

  ```
  $ cd ~/dotfiles
  $ ./bootstrap
  ```
